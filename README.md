# Adding New device in pv-initial devices

#### Preparing a device as reference for new initial device
1. Please follow the instructions for [Building Pantavisor](https://docs.pantahub.com/build-pantavisor/) image from source, [Flashing the image](https://docs.pantahub.com/image-setup-rpi3/#flash-initial-image) to SD card and [How to Claim a Device](https://docs.pantahub.com/claim-device/), then move to next step.
2. Fast copy `pvr-sdk`, `pv-avahi` and latest BSP from matching latest target BSP device, e.g.  
    - `pvr fastcopy -m "copy pvr-sdk from bpi_r64_initial_stable" https://pvr.pantahub.com/pantahub-ci/bpi_r64_initial_stable#pvr-sdk https://pvr.pantahub.com/gauravpathak/routinely_concrete_jennet`
    - `pvr fastcopy -m "copy pv-avahi from bpi_r64_initial_stable" https://pvr.pantahub.com/pantahub-ci/bpi_r64_initial_stable#pv-avahi https://pvr.pantahub.com/gauravpathak/routinely_concrete_jennet`
    - `pvr fastcopy -m "copy BSP from arm_bpi_r64_bsp_latest" https://pvr.pantahub.com/pantahub-ci/arm_bpi_r64_bsp_latest/53#bsp https://pvr.pantahub.com/gauravpathak/routinely_concrete_jennet`
3. Optionally rename the device to reflect the target and platform name.
4. Make the device public.

#### Adding files in the `pv-initial-devices` repository for checking, building, and updating the initial device whenever a tag is created.
1. Clone `pv-initial-devices` repository
    `git clone --recursive --recurse-submodules https://gitlab.com/pantacor/ci/pv-initial-devices.git`
2. Create a JSON file in the `recipes` directory with the name matching with the initial device name, e.g. if the initial device name is `bpi_r64_openwrt_latest` then create a file `bpi_r64_openwrt_latest.json` with the following content **(make sure that the revision is set to 0)**:
```
{
  "device": "bpi_r64_openwrt_latest",
  "revision": "0"
}
```  
- The value of key `"device":` must match exactly with the name of latest initial device.  
- The value for key `"revision:"` for the new initial device must be `0`, because the CI job tries to clone this revision for the very fist upgrade and increments this value automatically.  
3. Now open `device-ci.conf.json` and add the lines similar to the following example content matching the new device name preferably at the end or just below the matching device that already exists:
```
  {   
      "id":"bpi_r64_openwrt_latest",
      "schedule":"bpi_r64_openwrt_latest",
      "tag":"bpi_r64_openwrt_stable",
      "build": [
        {
          "name":"default",
          "options":"PANTAVISOR_DEBUG=yes"
        }
      ]   
    },
```
 - The value of key `"id":` and `"schedule":` must match exactly with the name of latest initial device.  
 - The value of `"tag":` must match exactly with the name of stable initial device.  
4. Now execute `./device-ci/mkdevice-ci.sh install` inside `pv-initial-devices` directory to update `.gitlab-ci.yml`, commit and push the changes.
5. Goto [CI Pipeline Schedule](https://gitlab.com/pantacor/ci/pv-initial-devices/-/pipeline_schedules) and click on the play button for **nightly upgrade recipes** after previous CI pipeline finishes.
6. As a final step create a release tag in `pv-initial-devices` repository after all the previous pipeline finishes successfully.
